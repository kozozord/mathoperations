/*
 * MultiplicationNode.h
 *
 *  Created on: 04-11-2018
 *      Author: tomek
 */

#ifndef MULTIPLICATIONNODE_H_
#define MULTIPLICATIONNODE_H_

#include "OperationNode.h"

class MultiplicationNode : public OperationNode  {
public:
	MultiplicationNode(char oper, shared_ptr<Node> a_left, shared_ptr<Node> a_right);
	virtual ~MultiplicationNode();

	int evaluate() override;
	string toStringValue() override;
};

#endif /* MULTIPLICATIONNODE_H_ */
