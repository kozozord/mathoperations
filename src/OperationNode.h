/*
 * OperationNode.h
 *
 *  Created on: 04-11-2018
 *      Author: tomek
 */

#ifndef OPERATIONNODE_H_
#define OPERATIONNODE_H_
#include "Node.h"

using namespace std;

class OperationNode : public Node {
protected:

	shared_ptr<Node> left;
	shared_ptr<Node> right;
public:
	OperationNode(char oper, shared_ptr<Node> a_left, shared_ptr<Node> a_right);
	OperationNode() {};

	virtual int evaluate() = 0;
	virtual string toStringValue() = 0;

	virtual ~OperationNode();
};

#endif /* OPERATIONNODE_H_ */
