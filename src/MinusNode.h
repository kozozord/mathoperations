/*
 * MinusNode.h
 *
 *  Created on: 04-11-2018
 *      Author: tomek
 */

#ifndef MINUSNODE_H_
#define MINUSNODE_H_

#include "OperationNode.h"

class MinusNode : public OperationNode {
public:
	MinusNode(char oper, shared_ptr<Node> a_left, shared_ptr<Node> a_right);
	virtual ~MinusNode();

	int evaluate() override;
	string toStringValue() override;
};

#endif /* MINUSNODE_H_ */
