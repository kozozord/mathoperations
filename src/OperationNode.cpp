/*
 * OperationNode.cpp
 *
 *  Created on: 04-11-2018
 *      Author: tomek
 */

#include "OperationNode.h"

OperationNode::OperationNode(char oper, shared_ptr<Node> a_left, shared_ptr<Node> a_right) : Node(oper), left(a_left), right(a_right) {

}

OperationNode::~OperationNode() {
}

