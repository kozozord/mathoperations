/*
 * ValueNode.cpp
 *
 *  Created on: 24-10-2018
 *      Author: tomek
 */

#include "ValueNode.h"

ValueNode::ValueNode(int val) : Node(), value(val) {

}

int ValueNode::evaluate()
{
	return value;
}

string ValueNode::toStringValue()
{
	return to_string(value);
}

ValueNode::~ValueNode() {
	// TODO Auto-generated destructor stub
}

