/*
 * AdditionNode.cpp
 *
 *  Created on: 04-11-2018
 *      Author: tomek
 */

#include "AdditionNode.h"

AdditionNode::AdditionNode(char oper, shared_ptr<Node> a_left, shared_ptr<Node> a_right) : OperationNode(oper, a_left, a_right) {

}

AdditionNode::~AdditionNode() {

}

int AdditionNode::evaluate()
{
	return (left->evaluate() + right->evaluate());
}
string AdditionNode::toStringValue()
{
	return left->toStringValue() + "+" + right->toStringValue();
}

