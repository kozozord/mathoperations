/*
 * AdditionNode.h
 *
 *  Created on: 04-11-2018
 *      Author: tomek
 */

#ifndef ADDITIONNODE_H_
#define ADDITIONNODE_H_

#include "OperationNode.h"

class AdditionNode : public OperationNode {
public:
	AdditionNode(char oper, shared_ptr<Node> a_left, shared_ptr<Node> a_right);
	virtual ~AdditionNode();

	int evaluate() override;
	string toStringValue() override;
};

#endif /* ADDITIONNODE_H_ */
