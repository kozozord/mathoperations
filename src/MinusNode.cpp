/*
 * MinusNode.cpp
 *
 *  Created on: 04-11-2018
 *      Author: tomek
 */

#include "MinusNode.h"

MinusNode::MinusNode(char oper, shared_ptr<Node> a_left, shared_ptr<Node> a_right) : OperationNode(oper, a_left, a_right)
{

}

MinusNode::~MinusNode() {
	// TODO Auto-generated destructor stub
}

int MinusNode::evaluate()
{
	return (left->evaluate() - right->evaluate());
}
string MinusNode::toStringValue()
{
	return left->toStringValue() + "-" + right->toStringValue();
}
