/*
 * Node.h
 *
 *  Created on: 23-10-2018
 *      Author: tomek
 */

#ifndef NODE_H_
#define NODE_H_

#include "iostream"
#include "string"
#include "memory"

using namespace std;

class Node {
public:
	char sign;

private:


public:
	Node(char oper) : sign(oper) {};
	Node() {};

	virtual ~Node();

	virtual int evaluate() = 0;
	virtual string toStringValue() = 0;

};

#endif /* NODE_H_ */
