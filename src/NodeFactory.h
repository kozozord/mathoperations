/*
 * NodeFactory.h
 *
 *  Created on: 30-10-2018
 *      Author: tomek
 */

#ifndef NODEFACTORY_H_
#define NODEFACTORY_H_
#include <memory.h>
#include "ValueNode.h"
#include "OperationNode.h"
#include "AdditionNode.h"
#include "MinusNode.h"
#include "MultiplicationNode.h"

class NodeFactory {
public:
	NodeFactory();
	static shared_ptr<ValueNode> createValue(int i)
	{
		return make_shared<ValueNode>(i);
	}

	// TODO make all operation as OperationNode
	static shared_ptr<AdditionNode> createAdditionOperation(char oper, shared_ptr<Node> a_left, shared_ptr<Node> a_right)
	{
        return make_shared<AdditionNode>(oper, a_left, a_right);
	}

	static shared_ptr<MinusNode> createMinusOperation(char oper, shared_ptr<Node> a_left, shared_ptr<Node> a_right)
	{
        return make_shared<MinusNode>(oper, a_left, a_right);
	}

	static shared_ptr<MultiplicationNode> createMultiplicationOperation(char oper, shared_ptr<Node> a_left, shared_ptr<Node> a_right)
	{
        return make_shared<MultiplicationNode>(oper, a_left, a_right);
	}

	virtual ~NodeFactory();
};

#endif /* NODEFACTORY_H_ */
