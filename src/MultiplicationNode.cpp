/*
 * MultiplicationNode.cpp
 *
 *  Created on: 04-11-2018
 *      Author: tomek
 */

#include "MultiplicationNode.h"

MultiplicationNode::MultiplicationNode(char oper, shared_ptr<Node> a_left, shared_ptr<Node> a_right) : OperationNode(oper, a_left, a_right) {

}

MultiplicationNode::~MultiplicationNode() {
	// TODO Auto-generated destructor stub
}

int MultiplicationNode::evaluate()
{
	return ((left->evaluate()) * (right->evaluate()));
}
string MultiplicationNode::toStringValue()
{
	string leftPart = left->toStringValue();
	if(left->sign == '+' || left->sign == '-')
	{
		leftPart = "(" + left->toStringValue() + ")";
	}

	string rightPart = right->toStringValue();
	if(right->sign == '+' || right->sign == '-')
	{
		rightPart = "(" + right->toStringValue() + ")";
	}

	return leftPart + "*" + rightPart;
}
