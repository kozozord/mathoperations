/*
 * ValueNode.h
 *
 *  Created on: 24-10-2018
 *      Author: tomek
 */

#ifndef VALUENODE_H_
#define VALUENODE_H_
#include "Node.h"

class ValueNode : public Node
{
public:

	int evaluate() override;
	string toStringValue() override;

	ValueNode(int val);
	virtual ~ValueNode();
private:
	int value;
};

#endif /* VALUENODE_H_ */
