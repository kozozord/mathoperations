/*
 * AllTests.cpp
 *
 *  Created on: 02-10-2018
 *      Author: tomek
 */

#include <stdio.h>
#include "gtest/gtest.h"
#include "iostream"

using namespace std;

int main(int argc, char **argv) {
  cout << "Running google tests" << endl;
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();

}



