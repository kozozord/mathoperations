#include "gtest/gtest.h"
#include "../src/Node.h"
#include "../src/NodeFactory.h"
#include "memory"


class NodeTest : public ::testing::Test
{
protected:
	shared_ptr<Node> value_1 = value(1);
	shared_ptr<Node> value_2 = value(2);
	shared_ptr<Node> value_3 = value(3);
public:
	 shared_ptr<Node> value(int i)
     {
        return NodeFactory::createValue(i);
     }

	 shared_ptr<Node> plus(shared_ptr<Node> left, shared_ptr<Node> right)
     {
        return NodeFactory::createAdditionOperation('+', left, right);
     }

	 shared_ptr<Node> minus(shared_ptr<Node> left, shared_ptr<Node> right)
     {
        return NodeFactory::createMinusOperation('-', left, right);
     }

	 shared_ptr<Node> multiplication(shared_ptr<Node> left, shared_ptr<Node> right)
     {
        return NodeFactory::createMultiplicationOperation('*', left, right);
     }
};


TEST_F(NodeTest, valueNodeEvaluateToIntegerValue)
{
	shared_ptr<Node> valueNode = value(1);
	ASSERT_EQ(1, valueNode->evaluate() );
}

TEST_F(NodeTest, simplePlusExpressionEvaluatesToIntegerValue)
{
	shared_ptr<Node> onePlusOne = plus(value_1, value_1 );
	ASSERT_EQ(2, onePlusOne->evaluate() );

	shared_ptr<Node> onePlusTwo = plus(value_1, value_2 );
	ASSERT_EQ(3, onePlusTwo->evaluate() );
}

TEST_F(NodeTest, simpleMultiplicationExpressionEvaluatesToIntegerValue)
{
	shared_ptr<Node> OneTimesThree = multiplication(value_1, value_3);
	ASSERT_EQ(3, OneTimesThree->evaluate() );

	shared_ptr<Node> TwoTimesThree = multiplication(value_2, value_3 );
	ASSERT_EQ(6, TwoTimesThree->evaluate() );
}

TEST_F(NodeTest, complexExpressionEvaluatesToIntegerValue)
{
	shared_ptr<Node> TwoTimesThree = multiplication(value_2, value_3 );
	ASSERT_EQ(6, TwoTimesThree->evaluate() );

	shared_ptr<Node> onePlusTwoTimesThree = plus(value_1, TwoTimesThree);
	ASSERT_EQ(7, onePlusTwoTimesThree->evaluate() );
}
//
TEST_F(NodeTest, topLevelPlusNeedsNoBrackets)
{
	shared_ptr<Node> TwoTimesThree = multiplication(value_2, value_3);
	ASSERT_EQ("2*3", TwoTimesThree->toStringValue() );

	shared_ptr<Node> onePlusTwoTimesThree = plus(value_1, TwoTimesThree);
	ASSERT_EQ("1+2*3", onePlusTwoTimesThree->toStringValue() );
}

TEST_F(NodeTest, lowLevelPlusNeedsBrackets)
{
	shared_ptr<Node> onePlusTwo = plus(value_1, value_2);
	shared_ptr<Node> onePlusTwoTimesThree = multiplication(onePlusTwo, value_3);
	ASSERT_EQ("(1+2)*3", onePlusTwoTimesThree->toStringValue() );
}

TEST_F(NodeTest, oneMinusTwoTimesThreeEvaluatesToMinusFive)
{
    shared_ptr<Node> twoTimesThree = multiplication(value_2, value_3);
    ASSERT_EQ(6, twoTimesThree->evaluate() );

    shared_ptr<Node> oneMinusTwoTimesThree = minus(value_1, twoTimesThree);
    ASSERT_EQ(-5, oneMinusTwoTimesThree->evaluate() );
}

TEST_F(NodeTest, minusExpressionMustHaveParenthesisIfParentOperationIsMultiplication)
{
    shared_ptr<Node> oneMinusTwo = minus(value_1, value_2);
    ASSERT_EQ(-1, oneMinusTwo->evaluate() );

    shared_ptr<Node> oneMinusTwoTimesThree = multiplication(oneMinusTwo, value_3);
    ASSERT_EQ(-3, oneMinusTwoTimesThree->evaluate());
    ASSERT_EQ("(1-2)*3", oneMinusTwoTimesThree->toStringValue());
}

